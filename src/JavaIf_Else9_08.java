public class JavaIf_Else9_08 {
    public static void main(String[] args) {
        int times1 = 20;
        if (times1 < 18) {
            System.out.println("Good day.");
        } else {
            System.out.println("Good evening.");
        }
    }
}
