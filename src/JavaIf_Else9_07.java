public class JavaIf_Else9_07 {
    public static void main(String[] args) {
        int times = 22;
        if (times < 10) {
            System.out.println("Good morning.");
        } else if (times < 20) {
            System.out.println("Good day.");
        } else {
             System.out.println("Good evening.");
        }
    }
}
