public class JavaOperators5 {
    public static void main(String[] args) {
        //Ex 1
        int x = 100 + 50;
        System.out.println(x);

        
        //Ex 2
        int sum1 = 100 + 50;
        int sum2 = sum1 + 250;
        int sum3 = sum2 + sum2;
        System.out.println(sum1);
        System.out.println(sum2);
        System.out.println(sum3);


        //Ex 3
        int z = 5;
        int y = 3;
        System.out.println(z + y);
        System.out.println(z - y);
        System.out.println(z * y);
        System.out.println(z / y);
        System.out.println(z % y);
        System.out.println(z + y);


        //Ex 4
        int w = 5;
        ++w;
        System.out.println(w);


        //Ex 5
        int v = 5;
        --v;
        System.out.println(v);


        //Ex 6
        int a = 10;
        a += 5;
        System.out.println(a);
        a -= 5;
        System.out.println(a);
        a *= 5;
        System.out.println(a);
        a /= 5;
        System.out.println(a);
        a %= 5;
        System.out.println(a);
        a &= 5;
        System.out.println(a);
        a |= 5;
        System.out.println(a);
        a ^= 5;
        System.out.println(a);
        a >>= 5;
        System.out.println(a);
        a <<= 5;
        System.out.println(a);


        //Ex 7
        int c = 5;
        int d = 3;
        System.out.println(c == d);
        System.out.println(c != d);
        System.out.println(c > d);
        System.out.println(c < d);
        System.out.println(c >= d);
        System.out.println(c <= d);

        //Ex 8
        int m = 5;
        System.out.println(m > 3 && m < 10);
        System.out.println(m < 5 || m < 4);
        System.out.println(!(m < 5 && m < 10));

    }
}
