public class JavaIf_Else9 {
    public static void main(String[] args) {
        //if (condition) {
            // block of code to be executed if the condition is true
        //}

        //if
        if (20 > 18) {
            System.out.println("20 is greater than 18"); // obviously    
        }

        //if
        int x = 20;
        int y = 18;
        if (x > y) {
            System.out.println("x is greater than y");
        }



        //if (condition) {
            // block of code to be executed if the condition is true
          //} else {
            // block of code to be executed if the condition is false
          //}

        //else
        int time = 20;
        if (time < 18) {
          System.out.println("Good day.");
        } else {
          System.out.println("Good evening.");
        }



        //if (condition1) {
            // block of code to be executed if condition1 is true
          //} else if (condition2) {
            // block of code to be executed if the condition1 is false and condition2 is true
          //} else {
            // block of code to be executed if the condition1 is false and condition2 is false
          //}

        //else if
        int times = 22;
        if (times < 10) {
            System.out.println("Good morning.");
        } else if (times < 20) {
            System.out.println("Good day.");
        } else {
             System.out.println("Good evening.");
        }



        int times1 = 20;
        if (times1 < 18) {
            System.out.println("Good day.");
        } else {
            System.out.println("Good evening.");
        }


        int times2 = 20;
        String result;
        result = (times2 < 18) ? "Good day." : "Good evening.";
        System.out.println(result);
    }
}
