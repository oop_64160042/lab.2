public class JavaTypeCasting4_02 {
    public static void main(String[] args) {
        System.out.println("Narrowing Casting");
        double myDouble2 = 9.78d;
        int myInt2 = (int) myDouble2; // Manual casting: double to int
          
        System.out.println(myDouble2);   // Outputs 9.78
        System.out.println(myInt2);      // Outputs 9
    }
}
