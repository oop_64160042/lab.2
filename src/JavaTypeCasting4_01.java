public class JavaTypeCasting4_01 {
    public static void main(String[] args) {
        System.out.println("Widening Casting");
        int myInt = 9;
        double myDouble = myInt; // Automatic casting: int to double

        System.out.println(myInt);
        System.out.println(myDouble);
    }
}
