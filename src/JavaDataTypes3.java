public class JavaDataTypes3 {
    public static void main(String[] args) {
        byte myNbyte = 100;
        System.out.println(myNbyte);

        short myNshort = 5000;
        System.out.println(myNshort);

        int myNint = 200000;
        System.out.println(myNint);

        long myNlong = 15000000000L;
        System.out.println(myNlong);

        float myFloat = 5.75f;
        System.out.println(myFloat);

        double myNdouble = 19.99d;
        System.out.println(myNdouble);

        float f1 = 35e3f;
        double d1 = 12E4d;
        System.out.println(f1);
        System.out.println(d1);

        boolean isJavaFun = true;
        boolean isFishTasty = false;
        System.out.println(isJavaFun);
        System.out.println(isFishTasty);

        char myGrade = 'A';
        System.out.println(myGrade);

        char myVar1 = 65, myVar2 = 66, myVar3 = 67;
        System.out.println(myVar1);
        System.out.println(myVar2);
        System.out.println(myVar3);

        String greeting = "Hello World";
        System.out.println(greeting);
    }
}
