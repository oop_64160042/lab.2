public class JavaOperators5_08 {
    public static void main(String[] args) {
        int m = 5;
        System.out.println(m > 3 && m < 10);
        System.out.println(m < 5 || m < 4);
        System.out.println(!(m < 5 && m < 10));
    }
}
