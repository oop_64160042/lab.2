public class JavaStrings6 {
    public static void main(String[] args) {
        String greeting = "Hello";
        System.out.println(greeting);


        String txt = "Hello my name is Chanisata Chunoparat";
        System.out.println("The length of the txt string is: " + txt.length());


        String txt2 = "Hello World";
        System.out.println(txt2.toUpperCase());   // Outputs "HELLO WORLD"
        System.out.println(txt2.toLowerCase());   // Outputs "hello world"


        String txt3 = "Please locate where 'locate' occurs!";
        System.out.println(txt3.indexOf("locate")); // Outputs 7

        String firstName = "Chanisata";
        String lastName = "Chunoparat";
        System.out.println(firstName + " " + lastName);

        String firstName1 = "Chanisata ";
        String lastName1 = "Chunoparat";
        System.out.println(firstName1.concat(lastName1));


        //String txt4 = "We are the so-called "Vikings" from the north.";
        String txt5 = "We are the so-called \"Vikings\" from the north.";
        System.out.println(txt5);

        String txt6 = "It\'s alright.";
        System.out.println(txt6);

        String txt7 = "The character \\ is called backslash.";
        System.out.println(txt7);

        String txt8 = "Hello\nWorld!";
        System.out.println(txt8);

        String txt9 = "Hello\rWorld!";
        System.out.println(txt9);

        String txt10 = "Hello\tWorld!";
        System.out.println(txt10);

        String txt11 = "Hel\blo World!";
        System.out.println(txt11);

        int x = 10;
        int y = 20;
        int z = x + y;      // z will be 30 (an integer/number)
        System.out.println(z);

        String w = "10";
        String v = "20";
        String t = w + v;
        System.out.println(t);

        String a = "10";
        int b = 20;
        String c = a + b;   // z will be 1020 (a String)
        System.out.println(c);
    }
}
