public class JavaIf_Else9_05 {
    public static void main(String[] args) {
        int time = 20;
        if (time < 18) {
          System.out.println("Good day.");
        } else {
          System.out.println("Good evening.");
        }
    }
}
