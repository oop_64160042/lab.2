public class JavaIf_Else9_09 {
    public static void main(String[] args) {
        int times2 = 20;
        String result;
        result = (times2 < 18) ? "Good day." : "Good evening.";
        System.out.println(result);
    }
}
