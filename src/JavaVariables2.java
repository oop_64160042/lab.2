public class JavaVariables2 {
    public static void main(String[] args) {
        //value
        
        String name = "MyMint";
        System.out.println(name);

        int MyAge = 20;
        System.out.println(MyAge);

        int myNum;
        myNum = 13;
        System.out.println(myNum);

        int myNums = 13;
        myNums = 20;
        System.out.println(myNums);

        //final int myNum = 15;
        //myNum = 20;
        // will generate an error: cannot assign a value to a final variable

        int Num = 5;
        float FloatNum = 5.99f;
        char Letter = 'D';
        boolean Bool = true;
        String Text = "Hello";
        System.out.println(Num);
        System.out.println(FloatNum);
        System.out.println(Letter);
        System.out.println(Bool);
        System.out.println(Text);

        String Myname = "My name is MyMint";
        System.out.println("Hello " + Myname);

        String firstName = "Chanisata ";
        String lastName = "Chunoparat";
        String fullName = firstName + lastName;
        System.out.println(fullName);

        int x = 5;
        int y = 4;
        System.out.println(x + y);

        int a = 5, b = 14, c = 60;
        System.out.println(a + b + c);

        int minutesPerHour = 79;
        int m = 79; 
        System.out.println(minutesPerHour);
        System.out.println(m);
    }
}
