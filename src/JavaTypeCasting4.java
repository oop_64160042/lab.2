public class JavaTypeCasting4 {
    public static void main(String[] args) {
    //Ex 1 เปลี่ยนจน.เต็มเป็นทศนิยม
    System.out.println("Widening Casting");
    int myInt = 9;
    double myDouble = myInt; // Automatic casting: int to double

    System.out.println(myInt);
    System.out.println(myDouble);

    //Ex 2 เปลี่ยนทศนิยมเป็นจน.เต็ม
    System.out.println("Narrowing Casting");
    double myDouble2 = 9.78d;
    int myInt2 = (int) myDouble2; // Manual casting: double to int
      
    System.out.println(myDouble2);   // Outputs 9.78
    System.out.println(myInt2);      // Outputs 9


    }
}
