public class JavaOperators5_07 {
    public static void main(String[] args) {
        int c = 5;
        int d = 3;
        System.out.println(c == d);
        System.out.println(c != d);
        System.out.println(c > d);
        System.out.println(c < d);
        System.out.println(c >= d);
        System.out.println(c <= d);
    }
}
